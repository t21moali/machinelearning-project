import unittest
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
from main import Logistic_regression  # Here, we import the function we are going to test

class TestLogisticRegression(unittest.TestCase):

    def test_Logistic_regression(self):

        X, y = make_classification(n_samples=200, n_features=5, n_informative=3, n_redundant=0, n_clusters_per_class=1, flip_y=0.03, class_sep=2, random_state=42)

        df = pd.DataFrame(X, columns=[f'feature{i}' for i in range(1, 6)])
        df['class'] = y

        train, test = train_test_split(df, test_size=0.2)

        model, score = Logistic_regression(train, test)

        # Check if the model returned is an instance of Logistic Regression
        self.assertIsInstance(model, LogisticRegression)

        # Check if the score is a float
        self.assertIsInstance(score, float)

if __name__ == '__main__':
    unittest.main()

"""
The execution of this script/unit test should display something like that, it allows us 
to be sure that the function works, giving both the best score and the best parameters : 
----------------------------------------------------------------------
Ran 1 test in 4.524s

OK
Best parameters: {'C': 0.1, 'penalty': 'l2', 'solver': 'liblinear'}
Best cross-validated score: 0.975
Test set score using the best model: 1.

"""