# Machine Learning Classification Project

## Project Description
This project involves preprocessing datasets, namely a kidney disease dataset and a banknote authentication dataset, and applying various machine learning classification methods such as K-Nearest Neighbors, Random Forest, Decision Tree, Multi-layer Perceptron, and Logistic Regression with hyperparameter tuning. The goal is to train and evaluate classifiers based on these datasets to predict specific outcomes.

## Datasets
1. **Kidney Disease Dataset**: Contains various parameters that can be used to predict chronic kidney disease.
2. **Banknote Authentication Dataset**: Consists of features extracted from images of genuine and forged banknotes.

## Features
- Data normalization and cleaning
- Training and evaluating models with hyperparameter tuning
- Modular code for easy understanding and reuse

## Requirements
- Python 3.x
- pandas
- scikit-learn
- numpy

Ensure you have the above requirements installed in your Python environment. You can install them using pip:

```
pip install numpy pandas scikit-learn
```

Ensure all dependencies of the main.py file and of the notebook are installed.

## Authors and contact

- MOALIC Tom tom.moalic@imt-atlantique.net ; 
- GRANDJACQUOT alexis.grandjacquot@imt-atlantique.net ; 
- GARFAGNI Maxime maxime.garfagni@imt-atlantique.net ; 
- LE ROY Thibault  thibault.le-roy@imt-atlantique.net ; 

