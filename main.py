import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV

############################################################################################################################################

                                                                # Main functions #
    
############################################################################################################################################ 

def clean_data(df_kidney,df_bank):

    ###Normalisation of df_bank
    df_bank["variance"]=minmax_norm(df_bank["variance"])
    df_bank["skewness"]=minmax_norm(df_bank["skewness"])
    df_bank["curtosis"]=minmax_norm(df_bank["curtosis"])
    df_bank["entropy"]=minmax_norm(df_bank["entropy"])
    
    ###df_kidney
    
    #removing id
    df_kidney.drop(["id"],axis=1,inplace=True)
    
    #rbc
    df_kidney["rbc"].fillna('normal', inplace=True)
    df_kidney["rbc"].replace("normal", True,inplace=True)
    df_kidney["rbc"].replace("abnormal", False,inplace=True)
    
    #pc
    df_kidney["pc"].fillna('normal', inplace=True)
    df_kidney["pc"].replace("normal", True,inplace=True)
    df_kidney["pc"].replace("abnormal", False,inplace=True)
    
    #pcc
    df_kidney["pcc"].fillna('notpresent', inplace=True)
    df_kidney["pcc"].replace("present", True,inplace=True)
    df_kidney["pcc"].replace("notpresent", False,inplace=True)
    
    #ba
    df_kidney["ba"].fillna('notpresent', inplace=True)
    df_kidney["ba"].replace("present", True,inplace=True)
    df_kidney["ba"].replace("notpresent", False,inplace=True)
    
    #pcv
    df_kidney["pcv"].replace("\t43", 43, inplace=True)

    ##pcv
    df_kidney["pcv"].replace("\t?", 0,inplace=True)
    df_kidney["pcv"].fillna(0, inplace=True)

    df_kidney["pcv"]=df_kidney["pcv"].astype(int)

    df_kidney["pcv"].replace(0, int(sum(df_kidney["pcv"])/(len(df_kidney["pcv"]))),inplace=True)

    ##wc
    df_kidney["wc"].fillna(0, inplace=True)
    df_kidney["wc"].replace("\t?", 0,inplace=True)
    df_kidney["wc"]=df_kidney["wc"].astype(int)
    df_kidney["wc"].replace(0, int(sum(df_kidney["wc"])/(len(df_kidney["wc"]))),inplace=True)

    ##rc
    df_kidney["rc"].fillna(0, inplace=True)
    df_kidney["rc"].replace("\t?", 0,inplace=True)
    df_kidney["rc"]=df_kidney["rc"].astype(float)
    df_kidney["rc"].replace(0, int(sum(df_kidney["rc"])/(len(df_kidney["rc"]))),inplace=True)

    #htn
    df_kidney["htn"].replace("no",False,inplace=True)
    df_kidney["htn"].replace("yes",True,inplace=True)
    df_kidney["htn"].fillna(False, inplace=True)
    
    ##dm
    df_kidney["dm"].replace("no",False,inplace=True)
    df_kidney["dm"].replace("\tno",False,inplace=True)
    df_kidney["dm"].replace("yes",True,inplace=True)
    df_kidney["dm"].replace("\tyes",True,inplace=True)
    df_kidney["dm"].replace(" yes",True,inplace=True)
    df_kidney["dm"].fillna(False, inplace=True)
    
    ##cad
    df_kidney["cad"].replace("no",False,inplace=True)
    df_kidney["cad"].replace("\tno",False,inplace=True)
    df_kidney["cad"].replace("yes",True,inplace=True)
    df_kidney["cad"].replace("\tyes",True,inplace=True)
    df_kidney["cad"].replace(" yes",True,inplace=True)
    df_kidney["cad"].fillna(False, inplace=True)
    
    ##appet
    df_kidney["appet"].fillna("good", inplace=True)
    df_kidney["appet"].replace("good",True,inplace=True)
    df_kidney["appet"].replace("poor",False,inplace=True)
    
    ##pe
    df_kidney["pe"].replace("no",False,inplace=True)
    df_kidney["pe"].replace("yes",True,inplace=True)
    df_kidney["pe"].fillna(False, inplace=True)
   
    ##ane
    df_kidney["ane"].replace("no",False,inplace=True)
    df_kidney["ane"].replace("yes",True,inplace=True)
    df_kidney["ane"].fillna(False, inplace=True)

    ##classification
    df_kidney["classification"].replace("ckd",True,inplace=True)
    df_kidney["classification"].replace("ckd\t",True,inplace=True)
    df_kidney["classification"].replace("notckd",False,inplace=True)
    
    
    ##replace Nan in float column
    df_kidney["age"].fillna(df_kidney["age"].mean(), inplace=True)
    df_kidney["bgr"].fillna(df_kidney["bgr"].mean(), inplace=True)
    df_kidney["bu"].fillna(df_kidney["bu"].mean(), inplace=True)
    df_kidney["bp"].fillna(df_kidney["bp"].mean(), inplace=True)
    df_kidney["al"].fillna(df_kidney["al"].mean(), inplace=True)
    df_kidney["su"].fillna(df_kidney["su"].mean(), inplace=True)
    df_kidney["sc"].fillna(df_kidney["sc"].mean(), inplace=True)
    df_kidney["sod"].fillna(df_kidney["sod"].mean(), inplace=True)
    df_kidney["pot"].fillna(df_kidney["pot"].mean(), inplace=True)
    df_kidney["hemo"].fillna(df_kidney["hemo"].mean(), inplace=True)
    df_kidney["pcv"].fillna(df_kidney["pcv"].mean(), inplace=True)
    df_kidney["wc"].fillna(df_kidney["wc"].mean(), inplace=True)
    df_kidney["rc"].fillna(df_kidney["rc"].mean(), inplace=True)
    df_kidney["sg"].fillna(df_kidney["sg"].mean(), inplace=True)


    ##Normalisation of df_Kidney
    df_kidney["age"]=minmax_norm(df_kidney["age"])
    df_kidney["bp"]=minmax_norm(df_kidney["bp"])
    df_kidney["sg"]=minmax_norm(df_kidney["sg"])
    df_kidney["al"]=minmax_norm(df_kidney["al"])
    df_kidney["su"]=minmax_norm(df_kidney["su"])
    df_kidney["bgr"]=minmax_norm(df_kidney["bgr"])
    df_kidney["bu"]=minmax_norm(df_kidney["bu"])
    df_kidney["sc"]=minmax_norm(df_kidney["sc"])
    df_kidney["sod"]=minmax_norm(df_kidney["sod"])
    df_kidney["pot"]=minmax_norm(df_kidney["pot"])
    df_kidney["hemo"]=minmax_norm(df_kidney["hemo"])
    df_kidney["pcv"]=minmax_norm(df_kidney["pcv"])
    df_kidney["wc"]=minmax_norm(df_kidney["wc"])
    df_kidney["rc"]=minmax_norm(df_kidney["rc"])


    df_kidney.rename(columns={"classification":"class"},inplace=True)

    return df_kidney , df_bank

def prepare_data(df_kidney,df_bank):
    
    df_kidney_Train,df_bank_Train=[],[]
    
    df_kidney_Train_sets_1, df_kidney_Test=train_test_split(df_kidney,test_size=0.25,random_state=41)
    df_kidney_Train_sets_2, df_kidney_Train_sets_3=train_test_split(df_kidney_Train_sets_1,test_size=0.5,random_state=41)
    df_kidney_Train_1, df_kidney_Train_2=train_test_split(df_kidney_Train_sets_2,test_size=0.5,random_state=41)
    df_kidney_Train_3, df_kidney_Train_4=train_test_split(df_kidney_Train_sets_3,test_size=0.5,random_state=41)
    df_kidney_Train.append(df_kidney_Train_1)
    df_kidney_Train.append(df_kidney_Train_2)
    df_kidney_Train.append(df_kidney_Train_3)
    df_kidney_Train.append(df_kidney_Train_4)
    
    df_bank_Train_sets_1, df_bank_Test=train_test_split(df_bank,test_size=0.25,random_state=41)
    df_bank_Train_sets_2, df_bank_Train_sets_3=train_test_split(df_bank_Train_sets_1,test_size=0.5,random_state=41)
    df_bank_Train_1, df_bank_Train_2=train_test_split(df_bank_Train_sets_2,test_size=0.5,random_state=41)
    df_bank_Train_3, df_bank_Train_4=train_test_split(df_bank_Train_sets_3,test_size=0.5,random_state=41)
    df_bank_Train.append(df_bank_Train_1)
    df_bank_Train.append(df_bank_Train_2)
    df_bank_Train.append(df_bank_Train_3)
    df_bank_Train.append(df_bank_Train_4)

    return df_kidney_Train,df_kidney_Test,df_bank_Train,df_bank_Test



### Function to call for training a binary classifier on the data
### Return the model and the score on the test data
def Train(Methods, Datasets):
    Classifiers = []
    Classifiers_average_score = []
    print(Methods)
    i = 1
    for Name, [Train, Test] in Datasets.items():
        print(f"\n \n-----------------------------------Training on the {Name}-----------------------------------")
        method_classifier = []
        method_score = []

        for method in Methods:
            print(f"\n-----------------------------------Training using a {method} algorithm-----------------------------------")
            average_score=0
            method_classifiers_list=[]
            for Train_set in Train:
                classifier, score = training(method, Train_set, Test)
                if(classifier is None): pass
                average_score+=score
                method_classifiers_list.append(classifier)
            average_score=average_score/4
            method_classifier.append(method_classifiers_list)
            method_score.append(average_score)

        Classifiers.append(method_classifier)
        Classifiers_average_score.append(method_score)
        i += 1

    return Classifiers, Classifiers_average_score

def display_results(df_bank_Test,Classifiers,method,train):
    
    n_method,n_train=None,None
    
    if method=="KNN":
        n_method=0
    elif method=="Random_forest":
        n_method=1
    elif method=="Decision_tree":
        n_method=2
    elif method=="MLP":
        n_method=3
    elif method=="Logistic regression":
        n_method=4
    else:
        print("Warning : The specified method is not recognized")
        return None
    
    if train in [1,2,3,4]:
        n_train=train-1
    else:
        print("Warning : The specified fraction of the dataset is not recognized")
        return None
    
    fig = plt.figure()
    ax = fig.add_subplot(projection="3d")
        
    df_bank_Test['Class_prediction'] = Classifiers[0][n_method][n_train].predict(df_bank_Test.drop("class", axis = 1))
    ax.scatter(df_bank_Test[(df_bank_Test['class'] == 1) & (df_bank_Test['Class_prediction'] == 1)]['variance'], 
               df_bank_Test[(df_bank_Test['class'] == 1) & (df_bank_Test['Class_prediction'] == 1)]['skewness'], 
               df_bank_Test[(df_bank_Test['class'] == 1) & (df_bank_Test['Class_prediction'] == 1)]["curtosis"], label = "Class 1 (Good pred)")

    ax.scatter(df_bank_Test[(df_bank_Test['class'] == 1) & (df_bank_Test['Class_prediction'] == 0)]['variance'], 
               df_bank_Test[(df_bank_Test['class'] == 1) & (df_bank_Test['Class_prediction'] == 0)]['skewness'], 
               df_bank_Test[(df_bank_Test['class'] == 1) & (df_bank_Test['Class_prediction'] == 0)]["curtosis"], label = "Class 1 (Wrong pred)", c = 'r')

    ax.scatter(df_bank_Test[(df_bank_Test['class'] == 0) & (df_bank_Test['Class_prediction'] == 0)]['variance'], 
               df_bank_Test[(df_bank_Test['class'] == 0) & (df_bank_Test['Class_prediction'] == 0)]['skewness'], 
               df_bank_Test[(df_bank_Test['class'] == 0) & (df_bank_Test['Class_prediction'] == 0)]["curtosis"], label = "Class 0 (Good pred)")

    ax.scatter(df_bank_Test[(df_bank_Test['class'] == 0) & (df_bank_Test['Class_prediction'] == 1)]['variance'], 
               df_bank_Test[(df_bank_Test['class'] == 0) & (df_bank_Test['Class_prediction'] == 1)]['skewness'], 
               df_bank_Test[(df_bank_Test['class'] == 0) & (df_bank_Test['Class_prediction'] == 1)]["curtosis"], label = "Class 0 (Wrong pred)", c = 'r')
    plt.title("Visualization of the BankNote authentification dataset")
    ax.set_xlabel("variance")
    ax.set_ylabel("skewsness")
    ax.set_zlabel("curtosis")
    plt.legend()
    plt.show()

    df_bank2 = df_bank_Test.copy()
    df_bank2["class"] = abs(df_bank_Test["class"] - df_bank_Test["Class_prediction"])
    df_bank2 = df_bank2.drop("Class_prediction", axis = 1)
    df_bank2["class"] = df_bank2["class"].map({0: "Well classified", 1: "Wrongly Classified"})
    sns.pairplot(df_bank2, hue = "class")

    return None
    

############################################################################################################################################

                                                           # Other useful functions #
    
############################################################################################################################################

def getData():
    df_kidney=pd.read_csv("kidney_disease.csv")
    df_bank=pd.read_csv("data_banknote_authentication.txt",sep=",",names=["variance","skewness","curtosis","entropy","class"])

    return df_kidney, df_bank

def minmax_norm(df_input):
    return (df_input - df_input.min()) / (df_input.max() - df_input.min())

### function selecting the training method
def training(Method, train_data, test_data):
    if(Method == "KNN"):
        Classifier, max_score = KNN(train_data, test_data)
    elif(Method == "Random_forest"):
        Classifier, max_score = Random_forest_classifier(train_data, test_data)
    elif(Method == "Decision_tree"):
        Classifier, max_score = Decision_tree_classifier(train_data, test_data)
    elif(Method == "MLP"):
        Classifier, max_score = MLP_classifier(train_data, test_data)
    elif(Method == "Logistic regression"):
        Classifier, max_score = Logistic_regression(train_data, test_data)
    else:
        print("Warning : The specified method is not recognized")
        return None, None

    return Classifier, max_score



############################################################################################################################################

                                                                # Methods #
    
############################################################################################################################################ 

def KNN(train,test):
    scores=[]
    for k in range(1,30):
        neigh = KNeighborsClassifier(n_neighbors=k)
        neigh.fit(train.drop(["class"],axis=1), train["class"])
        score=neigh.score(test.drop(["class"],axis=1), test["class"])
        scores.append(score)
        print(f"Score for KNN of with n_neighbors = {k} = {score}")

    max_score=max(scores)
    k_max=scores.index(max_score)+1
    
    neigh = KNeighborsClassifier(n_neighbors=k_max)
    neigh.fit(train.drop(["class"],axis=1), train["class"])
    
    
        
    return neigh, max_score

def Random_forest_classifier(train, test):
    scores=[]
    for i in range(1, 15):
        forest = RandomForestClassifier(max_depth = i, n_estimators = 1000)
        forest.fit(train.drop(["class"],axis=1), train["class"])
        score=forest.score(test.drop(["class"],axis=1), test["class"])
        scores.append(score)
        print(f"Score for random forest of depth {i} = {score}")
        if(score >= 1): break
    max_score=max(scores)
    
    best_depth=scores.index(max_score)+1
    
    best_forest = RandomForestClassifier(max_depth = best_depth, n_estimators = 1000)
    best_forest.fit(train.drop(["class"],axis=1), train["class"])

        
    return best_forest, max_score

def Decision_tree_classifier(train, test):
    scores=[]
    for i in range(1, 30):
        tree = DecisionTreeClassifier(max_depth = i)
        tree.fit(train.drop(["class"],axis=1), train["class"])
        score=tree.score(test.drop(["class"],axis=1), test["class"])
        scores.append(score)
        print(f"Score for tree classifier of depth {i} = {score}")
        if(score >= 1): break
    max_score=max(scores)
    
    best_depth=scores.index(max_score)+1
    
    best_tree = DecisionTreeClassifier(max_depth = best_depth)
    best_tree.fit(train.drop(["class"],axis=1), train["class"])

        
    return best_tree, max_score

def MLP_classifier(train, test):
    scores=[]
    for i in range(1, 50, 5):
        MLP = MLPClassifier(hidden_layer_sizes=(i,), max_iter=500)
        MLP.fit(train.drop(["class"],axis=1), train["class"])
        score=MLP.score(test.drop(["class"],axis=1), test["class"])
        scores.append(score)
        print(f"Score for MLP with {i} neuron = {score}")
        if(score >= 1): break
    max_score=max(scores)
    
    best_depth=scores.index(max_score)+1
    
    best_MLP = MLPClassifier(hidden_layer_sizes=(best_depth,), max_iter=500)
    best_MLP.fit(train.drop(["class"],axis=1), train["class"])

        
    return best_MLP, max_score

def Logistic_regression(train, test):
    LR = LogisticRegression()

    # Definition of a grid of parameters over which to optimize the logistic regression
    # Common parameters include 'C' for regularization strength and 'penalty' for the type of regularization
    param_grid = {
        'C': [0.001, 0.01, 0.1, 1, 10, 100],
        'penalty': ['l1', 'l2'],
        # Solver that supports the penalty we choose
        'solver': ['liblinear']
    }

    # Initialize GridSearchCV with the desired classifier, parameter grid, and number of cross-validation folds
    grid_search = GridSearchCV(LR, param_grid, cv=5, verbose=1, n_jobs=-1)

    # Fit the grid search model on the training data
    grid_search.fit(train.drop(["class"],axis=1), train["class"])

    best_params = grid_search.best_params_
    best_score = grid_search.best_score_

    print(f"Best parameters: {best_params}")
    print(f"Best cross-validated score: {best_score}")

    best_LR = grid_search.best_estimator_

    test_score = best_LR.score(test.drop(["class"],axis=1), test["class"])
    print(f"Test set score using the best model: {test_score}")

    return best_LR, test_score